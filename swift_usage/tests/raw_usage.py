#!/usr/bin/env python
import sys
from swift_usage.utils import db_connect # requires pymongo

if len(sys.argv) <> 4:
    print "Usage: python "+sys.argv[0]+" <account> <start> <end>"
    print " - <start> & <end> format is YYYYMMDDHH"
    sys.exit(1)

if len(sys.argv[2]) != 10 or len(sys.argv[3]) != 10 or sys.argv[2] > sys.argv[3]:
    print "Check the formatting and order of <start> and <end>."
    sys.exit(1)
    
db = db_connect.db
account = sys.argv[1]
start = sys.argv[2]
end = sys.argv[3]

print "------------------------------------------------------------"
print "-----  RAW USAGE COLLECTED FROM SLOGGING  ------------------"
print "------------------------------------------------------------"
print "-----  Account: "+account
print "-----  Start: "+start
print "-----  End: "+end
print "------------------------------------------------------------"

# handle the unique request for a single hour of data.
if start == end: # start and end dates are the same
    usage = db.usage.find_one({"account":account, "usage.hour":start})
    if usage:
        hourly_usage = [data for data in usage['usage'] if data['hour'] == start]
        if hourly_usage:
            print hourly_usage[0]['hour']+" => "+str(hourly_usage[0]['data'])
        else:
            print "No usage for this hour..."
            
# handle all requests other than the 'single hour' request.
if start < end:
    for month_data in db.usage.find({"account":account, "usage.hour":{"$gte":start}, "usage.hour":{"$lt":end}}):
        monthly_usage = [data for data in month_data['usage'] if data['hour'] >= start and data['hour'] < end]
        for usage in monthly_usage:
            print usage['hour']+" => "+str(usage['data'])
    
